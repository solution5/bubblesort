#include "iostream"

int main(int argc, char* argv[])
{
    // Value represent the size of a list
    const int ListSize = 10;

    //Initialization generator random values
    srand(2);

    // Create list
    int List[ListSize];

    // Create current value of the list
    int CurrentValue;

    std::cout << "The List before sort:\n |";
    //Show init list
    for (int i = 0; i < ListSize; i++)
    {
        List[i] = rand()%10;
        std::cout << List[i] << "|";
    }
    std::cout << std::endl;

    // Bubble sort
    for (int i = 0; i < ListSize - 1; i++) // Cycle for reaped operation sort
    {
        for (int j = 0; j < ListSize - 1; j++) // Cycle for go through by all values of the list 
        {
            if (List[j] > List[j + 1])
            {
                CurrentValue = List[j + 1];
                List[j + 1] = List[j];
                List[j] = CurrentValue;
            }
        }
    }

    std::cout << "The List after sort:\n |";
    // Show result of sort
    for(int &Result : List)
    {
        std::cout << Result << "|";
    }
    std::cout << std::endl;

    return 0;
}
